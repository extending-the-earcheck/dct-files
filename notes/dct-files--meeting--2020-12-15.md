# General comments

The definitions are relatively short and the instructions for developing measurement instruments is usually much longer. I think that part of the definition is currently implicitly embedded in the instructions. It would be good to delineate this and integrate the definition bits in the definition itself. However, that's not necessary for the systematic review yet.

The instructions sometimes read as a summary of the literature. Perhaps there should be a 'comments' field, as well?

For readability, maybe always use a paragraph break between the general introduction of qualitative research (with eliciting/developing aspects) and the specifics for a given construct?

# Comments per construct

## Action plans

Great; could be more specific, i.e. distinguishing the three subconstructs (where, when, how), but that's probably not feasible given our schedule.

## Action plan composite

Also excellent, very good to explicitly distinguish this.

## Action regret

Maybe add a link to anticipated regret as superconstruct, since much of the definition is located there. But that's a longer term issue; for moving ahead with the sysrev, this is fine I think.

## Anticipated regret

Very good. Part of the description can in principe be relocated to the corresponding DCTs, but that's a detail.

## Anticipated regret

Very good; good to have both versions, as reconciliation isn't straightforward. We'll have to decide which one to use for our qualitative and quantittaive research, though.

## Awareness

There is an inconsistency between the definition and the measurement instructions: the measurement instructions can convey the guideline, but the definition allows the construct to exist without any measurement, and as such, it should be able to exist also for people unaware of the guideline.

Also, in step 3 of measurement, I'd avoid dichotomization (even when over two dimensions).

## Behaviour

Very good!

## Behavioural script

The instruction for measurement seems to heavily overlap with habit. Maybe these should either merge, or more explicitly distinguish themselves. For example, behavioral scripts could be more generic, conform the examples in the instructions for developing measurement instruments (i.e. those cover scripts in general, e.g. in a bar, people normally buy each other drinks; in a restaurant, the waiter brings you the menu).

## Coping

DCT specifications should be self-contained; they should never refer people to other resources. DCTs exist to decrease ambiguity and increase unequivocality. Therefore, it is important that everybody who uses a given DCT works from exactly the same text and nothing is 'outsourced' to other documents. Ideally, we would have DCTs for every distinct coping style. For now, since none of the target behaviors seems to directly be a coping strategy, we can probably leave this out for now.

## Cues

Maybe add observation and field experiments for measurement? Not feasible for us, but does seem quite straightforward, no?

## Desire

Interesting. Works well like this, I think. Of course, one can have discussions about whether this is correct, but that's the point :-)

## Disengagement coping

Very good, I think this works excellently.

## Ego depletion

Very good, nice references to other constructs where fitting.

## Emotion

The measurement instruction suffers from the same problem as coping I think; it outsources the nitty gritty bits of the instructions to other sources. I think that ideally, it should be possible to develop a measurement instrument without consulting other sources (to minimize heterogeneity)

## Engagement coping

Very good, like disengagement coping.

## External regulation of behaviour

I think the measurement instructions are flawed (they require excellent introspection on the part of the participants, which I don't think is always feasible), but that's ok - they are definitely clear enough, which is the important thing.

## Goal

Very good.

## Habit

Also very good.

## Identified regulation of behaviour

I think that to be useful and unequivocal, the measurement instructions would need to be extended to provide the framework. But since I think the underlying approach is flawed anyway, it's fine to keep it like this for now. We'll have to look at this again if we decide we want to include this in the quantitative study.

## Impulse

The measurement explicitly positions this as the opposite of self-control; something to reflect on maybe.

## Inaction regret

Like action regret, very good.

## Biological influences

Excellent, really good (like the rest :-)).

## Demographic influences

Even better, really well worked out, especially the instructions for qualitative research!

## Environmental influences

See demography :-)

## Integrated regulation of behaviour

Well, uh, see comments at other SDT constructs :-)

## Intrinsic regulation of behaviour

See previous

## Introjected regulation of behaviour

See previous

## Knowledge

Excellent.

## Motivation

Very good - we shouldn't forget to replace the _XXX here.

## Need

The instructions for developing measurement instruments seem to misalign with the definition of the construct: e.g. "People at work care about me" does not measure whether "being cared about by people at work" is a need somebody has, or how strong that need is for that person; instead it measures the representation of that caring. If needs are axiomatic, and this construct is about 'need fulfilment', we should relabel it maybe (and adjust the definition so that it contains those needs)?

## Personality

This is ok. I'd normally say we should add the relevant subconstructs (e.g. the OCEAN ones etc) as well as the underlying subconstructs/facets. But since this is usually not considered modifiable anyway, we can code this at a pretty generic level. We can also flesh out the details if the sysrev shows that it's important.

## Personal norms

Excellent!

## Prototype

Should we explicitly mention that not performing a given behavior is also 'performing a behavior' and so also has a prototype?

## Prototype favourability

For qualitative research, also emphasize that the specific dimensions along which 'favourable evaluation' is perceived are also important to code (and then use when measuring favourability)?

## Prototype similarity

Very good!

## Risk perception

Very good.

## Self-control

Ah, right, the association to impulsivity is acknowledged here as well. Very good!

## Self-identity

Maybe list the items, to make the DCT more self-contained. We shouldn't forget to add the qualitative instructions!

## Self-monitoring

Maybe avoid the link in the present examples of measurement instruments, and focus on self-monitoring itself, regardless of goals?

For the qualitative instructions: the eliciting instructions assume self-monitoring, maybe rephrase/add intro question?

## Self Standards

Super-interesting. Very nice!

## Temptation

Hmm - this overlaps quite a bit with cues, no?

## Perceived threat severity

Really good.

## Perceived threat susceptibility

Also very good :-)

## Willingness

Very good as well.
