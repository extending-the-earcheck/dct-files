---
dct:
  version: 0.1.0
  id: referentApproval_73dnt5zd
  label: Perceived referent approval
  date: '2021-05-19'
  ancestry: ''
  retires: ''
  definition:
    definition: |
      The perceived approval or disapproval of a social referent refers to whether a given individual believes that social referent thinks that that given individual should engage in the target behavior. Note that this concerns whether the individual should engage in the target behavior, not whether the social referent themselves should engage in the target behavior. Also note that this is distinct from whether the social referent themselves does actually engage in the target behavior, which is captured in dct:referentBehavior_73dnt5zk.

      A social referent can be a specific individual, a group of distinct individuals (e.g. one’s siblings), or a more generically defined group (e.g. the residents of my city).

      The perceived approval for a given social referent combines multiplicatively with the motivation to comply with that referent (dct:motivationToComply_73dnt5zf) into an injunctive norm belief (dct:injNorms_belief_73dnt5zg).
  measure_dev:
    instruction: |
      Use item stems that list the target behavior and the social referent, and use disapproval and approval as anchors, in that order, with an intensifying adjective. Specifically, as item stem, use ‘If I were to (engage in) TARGET BEHAVIOR, [SOCIAL REFERENT] would…’ with anchors ‘strongly disapprove’ versus ‘strongly approve’. Note that the ‘engage in’ is optional, depending on the kind of behavior. For example, ‘If I were to engage in a demonstration, my neighbour would… [Strongly disapprove|Strongly approve]’

      Because this is a bidimensional scale, the scale midpoint reflects neutrality (i.e. one does not think the relevant social referent disapproves or approves of this target behavior. This means that a seven-point response scale is slightly preferred over a five-point response scale to allow participants to express three degrees of perceived (dis)approval.
  measure_code:
    instruction: 'Questions or questionnaires that measure the perceived degree to
      which a person’s salient normative referents approve or disapprove with the
      target behavior. A sample question can be: "If I were to (engage in) TARGET
      BEHAVIOR, [SOCIAL REFERENT] would…’ with anchors ‘Strongly disapprove’ versus
      ‘Strongly approve’. '
  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed,
      and the interviews are either recorded and transcribed, or notes are kept. These
      sources are then coded using the instruction for aspect coding.\n\nIn this qualitative
      study, we can elicit a person’s salient normative referents in a freeresponse
      format. For example, with respect to getting a mammogram in the next 2 weeks,
      respondents could be asked to list referents in response to the following: \"If
      you considered getting a mammogram in the next 2 weeks, there might be individuals
      or groups who would think you should or should not perform this behavior?\".\n
      \nAlternatively, respondents could be asked to answer a series of questions
      such as the following:\n1. Please name all people or groups who would approve
      of your getting, or who would encourage you to get, a mammogram in the next
      2 weeks.\n2. Please name all people or groups who would disapprove of your getting,
      or who would discourage you from getting, a mammogram in the next 2 weeks.\n3.
      Please name any other people or groups you might want to talk to if you were
      trying to decide whether or not to have a mammogram in the next 2 weeks.\n\nIn
      formulating these open-ended elicitation questions, it is essential to ensure
      compatibility in behavioral elements. That is, the behavior defined in the injunctive
      norm should be exactly replicated in eliciting salient referents. For example,
      if the injunctive norm is concerned with whether important others think I should
      wear jeans at work, then relevant referents should be elicited with respect
      to my wearing jeans at work and not with respect to my wearing jeans in general.\n"
  aspect_code:
    instruction: Expressions that demonstrate or imply the approval or disapproval
      of the target behaviour by a person’s salient normative referents.
  rel: ~

---

