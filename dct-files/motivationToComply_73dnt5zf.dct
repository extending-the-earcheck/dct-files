---
dct:
  version: 0.1.0
  id: motivationToComply_73dnt5zf
  label: Motivation to comply
  date: '2021-05-19'
  ancestry: ''
  retires: ''
  definition:
    definition: |
      Motivation to comply with a specific social referent refers to an individual’s motivation to behave in a way that is most approved of by that social referent. Note that motivation to comply is separate from the approval or disapproval itself, which is captured in dct:referentApproval_73dnt5zd.

      A social referent can be a specific individual, a group of distinct individuals (e.g. one’s siblings), or a more generically defined group (e.g. the residents of my city).

      The motivation to comply with a given social referent combines multiplicatively with the perceived approval of that referent (dct:referentApproval_73dnt5zd) into an injunctive norm belief (dct:injNorms_belief_73dnt5zg).
  measure_dev:
    instruction: |
      To measure the motivation to comply with a given social referent, measure how much people want to do what the relevant social referents think they should do with regard to the target behavior.

      In most behaviors and populations, unidimensional scales can be used. As item stem, use ‘When it comes to TARGET BEHAVIOR, I want to do what [SOCIAL REFERENT] think(s) I should do.’, and as anchors, ‘Not at all’ and ‘Very much’. For example, ‘When it comes to whether I avoid meat during dinner, I want to do what my partner thinks I should do. [’Not at all’|’Very much’]’ or ‘When it comes to my coffee consumption, I want to do what my colleagues think I should do. [’Not at all’|’Very much’]’.

      However, for some behaviors and some populations, it’s not only possible that individuals want to do what a social referent thinks they should do to a certain degree, but it’s also possible that the individual actively wants to do what that social referent would disapprove of. For example, people may strongly want to clearly demonstrate distancing themselves from members of a perceived outgroup (e.g. adolescents may want to disobey middle-aged people).

      In such situations, you can use a bidimensional scale to account for this variation. Use item stem ‘When it comes to TARGET BEHAVIOR, I want to do what [SOCIAL REFERENT]…’ with anchors ‘Do(es)n’t want me to do’ and ‘Want(s) me to do’. For example, ‘When it comes to condom use with a partner, I want to do what other adolescents… [Don’t want me to do|Want me to do]’, or ‘When it comes to how late I go to bed, I want to do what my older sibling … [Doesn’t want me to do|Wants me to do]’
  measure_code:
    instruction: 'Questions or questionnaires that measure how much people want to
      do what relevant social referents think they should do with regard to the target
      behavior. An example of a question measuring motivation to comply is: ''When
      it comes to TARGET BEHAVIOUR, I want to do what [NORMATIVE REFERENT] think(s)
      I should do’, and as anchors on a 7-point Likert scale, ‘Not at all’ and ‘Very
      much’.'
  aspect_dev:
    instruction: "Conduct a qualitative study, consisting of two stages, where participants
      are individually interviewed in a free-response format, and the interviews are
      either recorded and transcribed, or notes are kept. These sources are then coded
      using the instruction for aspect coding. \n\nIn the first stage of the study,
      a list of a person's salients normative referents with regard to the target
      behaviour is composed. This is done by asking an open-ended elicitation question
      such as 'There might be individuals or groups who would think you should or
      should not perform TARGET BEHAVIOUR. If any such individuals or groups come
      to mind when you think about TARGET BEHAVIOUR, please name them'. Alternatively,
      a series of open-ended questions could be asked: 1. Please list all people or
      groups who would approve of or who would encourage you to TARGET BEHAVIOUR;
      2. Please list all people or groups who would disapprove of or who would discourage
      you from TARGET BEHAVIOUR; 3. Please list any other people or groups you might
      want to talk to if you were trying to decide whether or not to TARGET BEHAVIOUR.
      Based on the responses to these elicitation questions a list of salients normative
      referents can be composed. \n\nIn the second stage of the qualitative study,
      for each referent on the list the participant is then asked to indicate how
      much he/she is motivated to behave in a way that is most approved of by that
      referent with regard to the target behaviour. An example a such a question is:
      ''When it comes to TARGET BEHAVIOUR, do you want to do what SALIENT NORMATIVE
      REFERENT thinks you should do?'. \n                                                                                                                                                                            "
  aspect_code:
    instruction: |
      Expressions that indicate the degree a participant is motivated to be behave in a way he/she thinks is approved by a salient normative referent with regards to the target behaviour.

      Expressions that refer to a participant's perception of the normative referent's approval or disapproval of the target behaviour, should be coded as dct:referentApproval_73dnt5zd.
  rel: ~

---

